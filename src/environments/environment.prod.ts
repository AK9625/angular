export const environment = {
  production: false
};

export const urls = {
  appBaseUrl: 'https://admin.inmenu.am',
  apiBaseUrl: "https://adminapi.inmenu.am/api",
}