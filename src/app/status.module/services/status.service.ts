import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { StatusModel } from '../models/StatusModel';

@Injectable()
export class StatusService extends CrudService<StatusModel> {

    protected serviceItemsKey = 'status';

    constructor(protected store: AppStore) {
        super('Status', null);
    }

    public preloadData() {
        return [
            this.getAllStatuses(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllStatuses() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }
}