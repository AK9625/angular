export class StatusModel {
	id: number;
	order: number;
	name: string; 
	colorInHex: string;
	nameTranslationId: number = 0;
	isActive: boolean = true;
}