// export class ResponseModel {
// 	data: any;
// 	hasError: boolean;
// 	errors: any;
// 	statusCode: number;
// }


import { Response } from '@angular/http';
export class ResponseModel {
    constructor() {
        this.errors = [];
        this.statusCode = 200;
        this.data = {};
        this.hasError = false;
    }

	data: any;
	hasError: boolean;
	errors: any;
	statusCode: number;

    public get isError(): Boolean {
        //console.log("apiresponse this ", this);
        return (this.statusCode >= 400 && this.statusCode <= 599);
    }

    static fromResponse(response) {

        let responseResult = new ResponseModel();
        if (response instanceof Response) {

            try {
                //console.log(response);
                let apiResult = JSON.parse(response["_body"]);
                //responseResult.isError = !response.ok;
                responseResult = <ResponseModel>apiResult;

            } catch (e) {
                //console.log("response", response)
                //console.log("Exception:", e)
                responseResult.errors = ["An error occured while processing request. Please contact administrator or try again later."];
                responseResult.statusCode = 500;
                responseResult.hasError = false;
            }
        }

        if (response.hasOwnProperty('data') && response.hasOwnProperty('statusCode') && response.hasOwnProperty('errors')) {
          
            responseResult.statusCode = response['statusCode'];
            responseResult.errors = response['errors'];
            responseResult.data = response['data'];
            responseResult.hasError = response['hasError'];
        }
        return responseResult;
    }

    // create(response) {
    //     ////console.log("response", response)
    //     let responseResult = new ResponseModel();
    //     if (response instanceof Response) {
    //         ////console.log("instance of Response");
    //         try {
    //             var apiResult = JSON.parse(response["_body"]);
    //             if (apiResult.hasOwnProperty('data') && apiResult.hasOwnProperty('httpCode') && apiResult.hasOwnProperty('messages')) {

    //             } else {
    //                 if (typeof apiResult == "string") {
    //                     responseResult.messages = [apiResult];
    //                 } else
    //                     if (typeof apiResult == "object") {
    //                         responseResult.data = apiResult;
    //                         responseResult.messages = [response.statusText];
    //                     }
    //             }
    //         } catch (e) {
    //             //console.log("Exception:", e)
    //             //console.log("response", response)
    //             responseResult.messages = ["An error occured while processing request. Please contact administrator or try again later."];
    //         }
    //     } else if (response instanceof ResponseModel) {
    //         responseResult = response;
    //         ////console.log(response);
    //     } else {
    //         responseResult = response.json();
    //     }
    //     return responseResult;
    // }
}