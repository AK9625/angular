﻿import { BaseComponent } from "./BaseComponent";

export abstract class PublicComponent extends BaseComponent {

    constructor() {
        super();
    }
}