import { Injectable } from "@angular/core";
import { DataStore } from "../models/DataStore";


@Injectable()
export class AppStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.clearAllData();
        //console.log('Initialize App store')

        // this.add('app', {
        //     reload: true,
        // });

        this.add('ui', {
            loading: false,
        });

        this.add('account', {
            isAuthenticated: () => {
                const tokenExistance = localStorage.getItem('Token');
                return !!tokenExistance;
            },
        });
    }
}