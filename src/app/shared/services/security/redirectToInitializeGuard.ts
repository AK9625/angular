import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AppStore } from '../store.service';

@Injectable()
export class RedirectToInitializeGuard implements CanActivate {
    constructor(private router: Router,
        private appStore: AppStore
    ) { }

    canActivate() {
        if (!this.appStore._('app.initialized')) {
            // if user is authenticated redirect to dashboard
            this.router.navigate(['/initialize']);
            return false;
        }
        return true;
    }
}