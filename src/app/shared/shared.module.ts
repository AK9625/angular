import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ToasterModule } from 'angular2-toaster/angular2-toaster';
import { BootstrapComponentsModule } from '../bootstrap-components.module/bootstrap-components.module';

import { PopoverModule } from 'ngx-bootstrap/popover';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HTTPListener } from './services/httplistener.service';

import { FlotDirective } from './directives/flot/flot.directive';
import { LoadingService } from './services/loading.service';
import { SparklineDirective } from './directives/sparkline/sparkline.directive';
import { EasypiechartDirective } from './directives/easypiechart/easypiechart.directive';
import { ColorsService } from './colors/colors.service';
import { CheckallDirective } from './directives/checkall/checkall.directive';
import { VectormapDirective } from './directives/vectormap/vectormap.directive';
import { NowDirective } from './directives/now/now.directive';
import { ScrollableDirective } from './directives/scrollable/scrollable.directive';
import { JqcloudDirective } from './directives/jqcloud/jqcloud.directive';
import { BusyScreenComponent } from './components/busy-screen/busy-screen.component';

import { BaseService } from './services/BaseService';
import { HttpClient } from './services/HttpClient';
import { StorageService } from './services/storage.service';
import { AppStore } from './services/store.service';
import { AlertsService } from './services/alerts.service';
import { RedirectToLoginGuard } from './services/security/redirectToLoginGuard';
import { RedirectToDashboardGuard } from './services/security/redirectToDashboardGuard';
import { RedirectToInitializeGuard } from './services/security/redirectToInitializeGuard';
import { AuthorizeGuard } from './services/security/authorizeGuard';

// https://angular.io/styleguide#!#04-10
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        DatepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        PopoverModule.forRoot(),
        ToasterModule,
        HttpModule,
        BootstrapComponentsModule
    ],
    providers: [
        ColorsService
    ],
    declarations: [
        FlotDirective,
        SparklineDirective,
        EasypiechartDirective,
        CheckallDirective,
        VectormapDirective,
        NowDirective,
        ScrollableDirective,
        JqcloudDirective,
        BusyScreenComponent
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        RouterModule,
        DatepickerModule,
        BsDatepickerModule,
        PopoverModule,
        ToasterModule,
        FlotDirective,
        SparklineDirective,
        EasypiechartDirective,
        CheckallDirective,
        VectormapDirective,
        NowDirective,
        ScrollableDirective,
        JqcloudDirective,
        HttpModule,
        BootstrapComponentsModule,
        BusyScreenComponent        
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                AppStore,
                BaseService,
                HttpClient,
                StorageService,
                AlertsService,
                LoadingService,
                RedirectToLoginGuard,
                RedirectToDashboardGuard,
                RedirectToInitializeGuard,
                AuthorizeGuard,
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: HTTPListener,
                    multi: true
                }
            ]
        };
    }
}
