import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { OrderModel } from '../models/OrderModel';

@Injectable()
export class OrderService extends CrudService<OrderModel> {

    protected serviceItemsKey = 'order';

    constructor(protected store: AppStore) {
        super('Order', null);
    }

     public preloadData() {
        return [
            this.getAllOrders(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllOrders(page = 1, pagesize = 10) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/GetAll?page=${page}&pagesize=${pagesize}`))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

    public getFiltered(filter:any) {
        return this.apirequest('post', this.apiCallTo(this.serviceApiUrl + `/GetFiltered`), null , JSON.stringify(filter))
            .pipe(
                map(i => i.data),
            );
    }

    private aaa(){
        return 111
    }

}