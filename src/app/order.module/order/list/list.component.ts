import { Component, OnInit, ViewEncapsulation } from '@angular/core';
//models
import { OrderModel } from '../../models/OrderModel';
import { KeyValuePair } from '../../../models/KeyValuePair';
//services
import { OrderService } from '../../services/order.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
declare var moment;

@Component({
	selector: 'order-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class OrderListComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private orderService: OrderService) {
		super();
	}

	public page: number = 1;
	public itemsPerPage: number = 10;
	public maxSize: number = 5;
	public numPages: number = 1;
	public length: number = 0;

	public orderFilter: OrderFilter = new OrderFilter();
	public orderList: Array<OrderModel> = [];
	public data: Array<OrderModel> = [];
	public statusList: Array<KeyValuePair> = [

	new KeyValuePair(0, 'New'),
	new KeyValuePair(1, 'Completed'),
	new KeyValuePair(2, 'Requested'),
	new KeyValuePair(3, 'Left'),

	];
    bsConfig = {
        containerClass: 'theme-angle'
    }
	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'table.place.name', name: 'Place Name' },
	{ title: 'table.name', name: 'Table Name' },
	{ title: 'user.name', name: 'User Name' },
	{ title: 'price', name: 'Amount' },
	{ title: 'status', name: 'Receipt Status' },
	{ title: 'createdDate', name: 'Open Date' },
	{ title: 'createdDate', name: 'Open Time' },
	{ title: 'settings', name: 'Settings' },

	];

	ngOnInit() {
		this.onChangeTable(this.config);
		this.orderService.getTotal().subscribe(res => {
			this.length = res;
		});
	}

	getdate(event){
      	this.orderFilter.startDate = event[0];
      	this.orderFilter.endDate = event[1];
	}

	public config: any = {
		paging: true,
		sorting: { columns: this.columns },
		filtering: { filterString: '' },
		className: ['table-striped', 'table-bordered', 'mb-0', 'd-table-fixed']
	};


	public filter() {
		this.orderFilter.amount = +this.orderFilter.amount
		this.orderService.getFiltered(this.orderFilter).subscribe(res=> {
			this.orderList = res;
		})
	}



	public onChangeTable(config: any, page: any = { page: this.orderFilter.page, itemsPerPage: this.itemsPerPage }): any {	
		this.orderService.getAllOrders(page.page).subscribe(res=>{
			this.orderList = res;

		});
	}

	public save(order: OrderModel ) {

		this.orderService.save(order).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Order updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}

export class OrderFilter{
	placeName: string;
	tableName: string;
	userName: string;
	amount: number;
	startDate: Date;
	endDate: Date;
	openTime: string;
	page: number = 1;
	pageSize: number = 10;
	isActive: boolean = true;
}