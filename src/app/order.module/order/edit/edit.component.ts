import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { OrderModel } from '../../models/OrderModel';
import { KeyValuePair } from '../../../models/KeyValuePair';
//services
import { OrderService } from "../../services/order.service";
import { ColorsService } from '../../../shared/colors/colors.service';
import { StatusService } from "../../../status.module/services/status.service";

//components
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'order-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class OrderEditComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		public orderService :OrderService,
		private activeRoute: ActivatedRoute,
		private statusService: StatusService,
        public colors: ColorsService) {
		super();
    }

	public order: OrderModel = new OrderModel();
    public typelist: Array<KeyValuePair> = [

        new KeyValuePair(0, 'Payment'),
        new KeyValuePair(1, 'AddCard'),
        new KeyValuePair(2, 'Cash'),

    ];

    public statusList: Array<KeyValuePair> = [

        new KeyValuePair(0, 'New'),
        new KeyValuePair(1, 'Completed'),
        new KeyValuePair(2, 'Requested'),
        new KeyValuePair(3, 'Left'),

    ];

	ngOnInit() {

		if (this.isEdit) {
			let orderId = this.activeRoute.snapshot.params['id'];
			this.orderService.get(orderId).subscribe(res => {
				if (!!res) {
					this.order = this.order.fromObject(res);
				}else {
					this.alertService.showError('Something went wrong!');
					this.back();
				}
			});
		}

	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid.');
			return false;
		}
		return true;
	}

	// public back() {
	// 	this.router.navigateByUrl("order/list");
	// }

    sparkOptions1 = {
        barColor: this.colors.byName('info'),
        height: 60,
        barWidth: 10,
        barSpacing: 6,
        chartRangeMin: 0
    };

    sparkOptions2 = {
        type: 'line',
        height: 60,
        width: '80%',
        lineWidth: 2,
        lineColor: this.colors.byName('purple'),
        chartRangeMin: 0,
        spotColor: '#888',
        minSpotColor: this.colors.byName('purple'),
        maxSpotColor: this.colors.byName('purple'),
        fillColor: '',
        highlightLineColor: '#fff',
        spotRadius: 3,
        resize: true
    };

    barStackedData: any;
    barStackedOptions = {
        series: {
            stack: true,
            bars: {
                align: 'center',
                lineWidth: 0,
                show: true,
                barWidth: 0.6,
                fill: 0.9
            }
        },
        grid: {
            borderColor: '#eee',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: '#fcfcfc'
        },
        tooltip: true,
        tooltipOpts: {
            content: (label, x, y) => { return x + ' : ' + y; }
        },
        xaxis: {
            tickColor: '#fcfcfc',
            mode: 'categories'
        },
        yaxis: {
            min: 0,
            max: 200, // optional: use it for a clear represetation
            // position: ($rootScope.app.layout.isRTL ? 'right' : 'left'),
            tickColor: '#eee'
        },
        shadowSize: 0
    };

    splineData: any;
    splineOptions = {
        series: {
            lines: {
                show: false
            },
            points: {
                show: true,
                radius: 4
            },
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.5
            }
        },
        grid: {
            borderColor: '#eee',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: '#fcfcfc'
        },
        tooltip: true,
        tooltipOpts: {
            content: (label, x, y) => { return x + ' : ' + y; }
        },
        xaxis: {
            tickColor: '#fcfcfc',
            mode: 'categories'
        },
        yaxis: {
            min: 0,
            max: 150, // optional: use it for a clear represetation
            tickColor: '#eee',
            // position: ($rootScope.app.layout.isRTL ? 'right' : 'left'),
            tickFormatter: (v) => {
                return v/* + ' visitors'*/;
            }
        },
        shadowSize: 0
    };


    easyPiePercent1 = 60;
    easyPiePercent2 = 30;
    easyPiePercent3 = 50;
    easyPiePercent4 = 75;

    pieOptions1 = {
        animate: {
            duration: 800,
            enabled: true
        },
        barColor: this.colors.byName('info'),
        trackColor: '#edf2f6',
        scaleColor: false,
        lineWidth: 2,
        lineCap: 'round',
        size: 130
    };
    pieOptions2 = {
        animate: {
            duration: 800,
            enabled: true
        },
        barColor: this.colors.byName('pink'),
        trackColor: '#edf2f6',
        scaleColor: false,
        lineWidth: 2,
        lineCap: 'round',
        size: 130
    };
    pieOptions3 = {
        animate: {
            duration: 800,
            enabled: true
        },
        barColor: this.colors.byName('purple'),
        trackColor: '#edf2f6',
        scaleColor: false,
        lineWidth: 2,
        lineCap: 'round',
        size: 130
    };
    pieOptions4 = {
        animate: {
            duration: 800,
            enabled: true
        },
        barColor: this.colors.byName('warning'),
        trackColor: '#edf2f6',
        scaleColor: false,
        lineWidth: 2,
        lineCap: 'round',
        size: 130
    };

}
