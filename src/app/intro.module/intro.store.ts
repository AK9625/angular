import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class IntroStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('intro', null);
    }
}