
export class UserProfileModel {
  id: number;
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
  imageUrl: string;
  phoneNumber: string;
  role: string;
  roles: Array<any> = [];
  countryId: number;
  organisation: string;
  lastName: string;
  firstName: string;
  isActive: boolean;
}