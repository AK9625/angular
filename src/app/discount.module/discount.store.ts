import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class DiscountStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('discount', null);
    }
}