import { Component, OnInit, Input , OnChanges } from '@angular/core';
//models
import { DiscountModel } from '../../models/DiscountModel';
//services
import { DiscountService } from '../../services/discount.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
  selector: 'discount-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class DiscountListComponent extends AuthenticatedComponent implements OnChanges {

	constructor(
		private discountService: DiscountService) {
		super()
	}

	@Input() placeId: number = null;
	@Input() discounts: Array<DiscountModel> = [];
	public discountList: Array<DiscountModel> = [];
	public data: Array<DiscountModel> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'username', name: 'username' },
	{ title: 'email', name: 'email' },
	{ title: 'percent', name: 'percent' },
	{ title: 'expireDate', name: 'expireDate' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnInit() {}
	ngOnChanges() {

		if(!this.placeId) {
			this.discountService.getAll().subscribe(data => {
				this.discountList = data;
			})
		}
	}

	public save(not: DiscountModel ) {

		this.discountService.save(not).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Discount updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}
