export class ContactModel{
	phoneNumber: number;
	websiteUrl:	string;
	isActive: boolean;
}