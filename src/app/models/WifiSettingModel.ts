export class  WifiSettingModel{
	wifiId: number;
	wifiName: string;
	wifiEncryption: number;
    wifiPassword: string;
	isActive: boolean;
}