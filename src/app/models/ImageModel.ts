export class ImageModel{
	id: number;
	isPrimary: boolean;
	url: string;
	isActive: boolean;
}