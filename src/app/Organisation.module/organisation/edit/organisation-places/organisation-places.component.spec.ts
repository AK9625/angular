import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationPlacesComponent } from './organisation-places.component';

describe('OrganisationPlacesComponent', () => {
  let component: OrganisationPlacesComponent;
  let fixture: ComponentFixture<OrganisationPlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationPlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationPlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
