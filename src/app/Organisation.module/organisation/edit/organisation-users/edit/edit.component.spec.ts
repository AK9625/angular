import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationUsersEditComponent } from './edit.component';

describe('OrganisationUsersEditComponent', () => {
  let component: OrganisationUsersEditComponent;
  let fixture: ComponentFixture<OrganisationUsersEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationUsersEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationUsersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
