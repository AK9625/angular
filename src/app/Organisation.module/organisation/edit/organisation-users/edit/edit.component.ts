import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//services
import { PlaceService } from "../../../../../place.module/services/place.service";
import { SectionService } from "../../../../../menu.module/services/menu.service";
import { OrganisationService } from "../../../../services/organisation.service";
import { AccountService } from '../../../../../account.module/services/account.service';
//models
import { KeyValuePair } from '../../../../../models/KeyValuePair';
import { RoleModel } from "../../../../../menu.module/models/SectionModel";
import { PlaceModel } from "../../../../../place.module/models/PlaceModel";
import { CreateUserModel } from '../../../../../account.module/models/CreateUserModel';
//components
import { AuthenticatedComponent } from '../../../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class OrganisationUsersEditComponent extends AuthenticatedComponent implements OnInit {

	public formId = 'user-form';
	public user: CreateUserModel = new CreateUserModel();
	public roleList: Array<RoleModel> = [];
	public placeList: Array<PlaceModel> = [];

	public genderList: Array<KeyValuePair> = [

		new KeyValuePair(1, 'Male'),
		new KeyValuePair(2, 'Female'),
		new KeyValuePair(3, 'Other'),
	];	

	constructor(
		private activeRoute: ActivatedRoute,
		private placeService: PlaceService,
		private sectionService: SectionService,
		private orgService: OrganisationService,
		private accountService: AccountService) {
		super(); 
	}

	ngOnInit(){

		this.orgService.getAllOrganisationalAsync().subscribe(res => {
			this.roleList = res;
		});

		let orgId = this.activeRoute.snapshot.params['organisationId'];
		this.user.organisationId = orgId;
		this.orgService.get(orgId).subscribe(res => {
			if (!!res) {
				this.placeList = res.places;
			}else {
				this.alertService.showError('Something went wrong!');
				this.back();
			}
		});
	}

	public getPlaces(event) {
		this.user.placeIds = event.map(i => i.key);
	}

	public getRoles(event) {
		this.user.roleIds = event.map(i => i.key);
	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid.');
			return false;
		}
		return true;
	}


	public save() {

		if (!this.isFormValid()) return;

		if (!this.user.id ) {

			this.accountService.createUser(this.user).subscribe(data => {
				if(!data.hasError) {
					this.alertService.showSuccess('User add successfully.');
					this.back();
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});	

		} else {

			this.accountService.save(this.user).subscribe(data => {
				if(!data.hasError) {
					this.alertService.showSuccess('User updated successfully.');
					this.back();
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});
		}
	}
}
