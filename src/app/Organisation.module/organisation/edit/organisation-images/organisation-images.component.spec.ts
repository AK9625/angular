import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationImagesComponent } from './organisation-images.component';

describe('OrganisationImagesComponent', () => {
  let component: OrganisationImagesComponent;
  let fixture: ComponentFixture<OrganisationImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
