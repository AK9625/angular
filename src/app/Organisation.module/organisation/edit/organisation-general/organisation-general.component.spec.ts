import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationGeneralComponent } from './organisation-general.component';

describe('OrganisationGeneralComponent', () => {
  let component: OrganisationGeneralComponent;
  let fixture: ComponentFixture<OrganisationGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
