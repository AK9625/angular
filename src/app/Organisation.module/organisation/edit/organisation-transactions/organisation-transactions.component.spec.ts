import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationTransactionsComponent } from './organisation-transactions.component';

describe('OrganisationTransactionsComponent', () => {
  let component: OrganisationTransactionsComponent;
  let fixture: ComponentFixture<OrganisationTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
