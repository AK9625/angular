import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { OrganisationModel } from '../../../models/OrganisationModel';
//services
import { OrganisationService } from "../../../services/organisation.service";
//components
import { AuthenticatedComponent } from '../../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-organisation-transactions',
	templateUrl: './organisation-transactions.component.html',
	styleUrls: ['./organisation-transactions.component.scss']
})
export class OrganisationTransactionsComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private activeRoute: ActivatedRoute,
		public orgService :OrganisationService) {
		super(); 
	}

	@Input() organisationId: number = null;
	public transactionList: Array<any> = [];
	public data: Array<any> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'userId', name: 'userId' },
	{ title: 'orderId', name: 'orderId' },
	{ title: 'amount', name: 'amount' },
	{ title: 'date', name: 'date' },

	];
	ngOnInit() {
		if (!!this.organisationId) {

			this.orgService.getAllTransactions(this.organisationId).subscribe(res=> {
				this.transactionList = res;
			})
		}
	}

}
