import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserProfileModel } from '../../account.module/models/UserProfileModel';

@Injectable()
export class ClientService extends CrudService<UserProfileModel> {

    protected serviceItemsKey = 'user';

    constructor(protected store: AppStore) {
        super('User', null);
    }

    public preloadData() {
        return [
            this.getAllUsers(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllUsers() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }
}