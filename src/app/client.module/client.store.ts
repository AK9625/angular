import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class ClientStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('user', null);
    }
}