import { Component, OnInit } from '@angular/core';
//models
import { UserProfileModel } from '../../../account.module/models/UserProfileModel';
//services
import { ClientService } from '../../services/client.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'client-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ClientListComponent extends AuthenticatedComponent implements OnInit {

	constructor(public clientService: ClientService) {
		super() 
	}

	public clientList: Array<UserProfileModel> = [];
	public data: Array<UserProfileModel> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'image', name: 'image' },
	{ title: 'username', name: 'username' },
	{ title: 'firstName', name: 'firstName'},
	{ title: 'lastName', name: 'lastName' },
	{ title: 'email', name: 'email'},
	{ title: 'settings', name: 'settings' },

	];

	ngOnInit() {
		this.clientService.getAll().subscribe(data => {
			this.clientList = data;
		});
	}

	public save(client: UserProfileModel ) {

		this.clientService.save(client).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Clinet Info updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}

}
