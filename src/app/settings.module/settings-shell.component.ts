import { Component, OnInit } from "@angular/core";
import { AuthenticatedComponent } from "../shared/components/AuthenticatedComponent";
@Component({
    template: `
    <a routerLink="/settings/privacy/list" class="btn btn-info btn-round mb-2 mr-2">
    <i class="fas fa-chair"></i>
    <span> Privacy </span>
    </a>
    <a routerLink="/settings/about-us/list" class="btn btn-info btn-round mb-2 mr-2">
    <i class="fas fa-chair"></i>
    <span> About Us </span>
    </a>
    <a routerLink="/settings/terms-of-use/list" class="btn btn-info btn-round mb-2 mr-2">
    <i class="fas fa-chair"></i>
    <span> Terms Of Use </span>
    </a>
    <router-outlet></router-outlet>
    `
})
export class SettingsShellComponent extends AuthenticatedComponent implements OnInit {

    constructor() {
        super();
    }

    ngOnInit() { }

}