export class PrivacyPolicyModel{
	id: number;
	policy:	string;
	aboutUs: string;
	termsOfUse: string;
	policyTranslationId:number = 0;
	termsOfUseTranslationId:number = 0;
	aboutUsTranslationId: number = 0;
	isActive: boolean = true;
}