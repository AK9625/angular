import { AuthorizeGuard } from "../shared/services/security/authorizeGuard";
import { RedirectToLoginGuard } from "../shared/services/security/redirectToLoginGuard";
//components
import { SettingsShellComponent } from './settings-shell.component';
import { PrivacyEditComponent } from './settings/privacy/edit/edit.component';
import { PrivacyListComponent } from './settings/privacy/list/list.component';
import { AboutEditComponent } from './settings/about/edit/edit.component';
import { AboutListComponent } from './settings/about/list/list.component';
import { TermsEditComponent } from './settings/terms/edit/edit.component';
import { TermsListComponent } from './settings/terms/list/list.component';

export const routes = [
  {
    path: "settings",
    component: SettingsShellComponent,
    canActivate: [RedirectToLoginGuard],
    data: {
      mustHave: {
        permissions: []
      }
    },
    children: [
     {
        path: "privacy/list",
        component: PrivacyListComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },     
      {
        path: "privacy/add",
        component: PrivacyEditComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },     
      {
        path: "privacy/edit/:id",
        component: PrivacyEditComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },
      {
        path: "about-us/list",
        component: AboutListComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },      
      {
        path: "about-us/add",
        component: AboutEditComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },      
      {
        path: "about-us/edit/:id",
        component: AboutEditComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },
      {
        path: "terms-of-use/list",
        component: TermsListComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },
      {
        path: "terms-of-use/add",
        component: TermsEditComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      },
      {
        path: "terms-of-use/edit/:id",
        component: TermsEditComponent,
        canActivate: [RedirectToLoginGuard],
        data: {
          mustHave: {
            permissions: []
          }
        }
      }
    ],
  }
]