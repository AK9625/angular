import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class SettingsStore extends DataStore {

    constructor() {
        super();
    }

    public initializeStore() {
        this.set('policy', null);
    }
}