import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class PropertyStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('property', null);
    }
}