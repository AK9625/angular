export class PropertyModele{
	id: number;
	text: string;
	state: boolean = true;
	price: number;
	optionId: number;
	textTranslationId: number = 0;
	isActive: boolean = true;
}