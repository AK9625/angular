import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { PropertyModele } from '../../models/PropertyModele';
//services
import { PropertyService } from '../../services/propery.services';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class PropertyListComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private activeRoute: ActivatedRoute,
		private propertyService: PropertyService) {
		super();
	}

	public propertyList: Array<PropertyModele> = [];
	public data: Array<PropertyModele> = [];
	public optionId: number = null;
	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'name', name: 'name' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnInit() {

		this.optionId = this.activeRoute.snapshot.params['optionId'];
		this.propertyService.GetAllByOptionId(this.optionId).subscribe(data => {
			this.propertyList = data;
		});
	}

	public save(property: PropertyModele ) {
		this.propertyService.save(property).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Property updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}