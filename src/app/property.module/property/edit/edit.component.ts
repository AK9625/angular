import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { PropertyModele } from '../../models/PropertyModele';
import { KeyValuePair } from '../../../models/KeyValuePair';
//services
import { PropertyService } from "../../services/propery.services";
import { OptionService } from "../../../option.module/services/option.service";
//components
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class PropertyEditComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private activeRoute: ActivatedRoute,
		private optionService: OptionService,
		private propertyService: PropertyService) {
		super();
	}

	public property: PropertyModele = new PropertyModele();
	public option: string = null;

	ngOnInit() {
		this.property.optionId = this.activeRoute.snapshot.params['optionId'];

		if (this.isEdit) {
			let propertyId = this.activeRoute.snapshot.params['id'];
			this.propertyService.get(propertyId).subscribe(res => {
				if (!!res) {
					this.property = res;
					this.optionService.get(this.property.optionId).subscribe(r => {
						this.option = r.title;
					});
				}else {
					this.alertService.showError('Something went wrong!');
					this.back();
				}
			});
		} else {
			this.optionService.get(this.property.optionId).subscribe(res => {
				this.option = res.title;
			});
		}
	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid.');
			return false;
		}
		return true;
	}

	// public back() {
	// 	this.router.navigateByUrl("property/list");
	// }

	public save() {

		if (!this.isFormValid()) return;

		if (!this.property.id ) {

			this.propertyService.save(this.property).subscribe(data => {
				if(!data.hasError) {
					this.alertService.showSuccess('Property add successfully.');
					this.propertyService.updateData({id:data.data}).subscribe(res => {
						this.back();
					})
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});	

		} else {

			this.propertyService.save(this.property).subscribe(data => {
				if(!data.hasError) {
					this.alertService.showSuccess('Property updated successfully.');
					this.propertyService.updateData({id:this.property.id}).subscribe(res => {
						this.back();
					})
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});
		}
	}
}