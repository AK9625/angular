import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "../shared/shared.module";
import { RedirectToLoginGuard } from '../shared/services/security/redirectToLoginGuard';
import { PropertyStore } from './property.store';
import { appInjector } from '../bootstrap-components.module/utils/appInjector';
import { AppStore } from '../shared/services/store.service';
import { routes } from './property.routes';

import { PropertyService } from "./services/propery.services";
import { PropertyShellComponent } from './property-shell.component';
import { PropertyListComponent } from './property/list/list.component';
import { PropertyEditComponent } from './property/edit/edit.component';


@NgModule({
  imports: [
  CommonModule,
  BrowserModule,
  SharedModule,
  RouterModule
  ],
  declarations: [
  PropertyShellComponent,
  PropertyListComponent,
  PropertyEditComponent,
  ],

  exports: [],
  entryComponents: [],

  providers: [ PropertyService ]
})
export class PropertyModule {

  static preloadData() {
    const services = this.moduleServices();
    return {
      resources: [
      ...services.propertyService.preloadData(),
      ]
    };
  }

  private static moduleServices() {
    let appinjector = appInjector.injector();
    let store = appinjector.get(AppStore);
    let propertyService = appinjector.get(PropertyService);

    return {
      store,
      propertyService,
    }
  }

  static initializeModuleData() {
    let injector = appInjector.injector();
    let store = injector.get(AppStore);
    PropertyStore.prototype.initializeStore.call(store);
    this.loadData();
  }

  static routerRoutes() {
    return routes;
  }

  static initializeStore(store) {
    PropertyStore.prototype.initializeStore.call(store);
  }

  public static loadData() {
    const services = this.moduleServices();
    services.propertyService.preloadData();
  }

}