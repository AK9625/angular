import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { PropertyModele } from '../models/PropertyModele';

@Injectable()
export class PropertyService extends CrudService<PropertyModele> {

    protected serviceItemsKey = 'property';

    constructor(protected store: AppStore) {
        super('Property', null);
    }

    public preloadData() {
        return [
            this.getAllProperty(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllProperty() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

    public GetAllByOptionId(id) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/GetAllByOptionId?optionId=${id}`))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }
}