import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthenticationService } from '../services/authentication.service';
import { PublicComponent } from '../../../shared/components/PublicComponent';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent extends PublicComponent implements OnInit  {

    valForm: FormGroup;

    constructor(public fb: FormBuilder,
        public settings: SettingsService,
        private route: ActivatedRoute,
        private authService: AuthenticationService) {
        super();
    }

    ngOnInit() {
        this.valForm = this.fb.group({
            'login': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'password': [null, Validators.required]
        });
    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            this.authService.login(value)
                .subscribe(
                    (response) => {
                        //var apiResponse = this.getApiResponseModel(response);
                        if (!response.data) {
                            let errors = response.errors.map(r => {return r.message});
                            this.alertService.showError(errors);
                        } else {
                            document.body.className = "";
                            this.alertService.showSuccess("Login with success");
                            //this.goToHomeUrl(returnUrl);
                            //this.goToDashboard();
                            this.goToUrl("/initialize")
                        }
                    });
        }
    }


}
