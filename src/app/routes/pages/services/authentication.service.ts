import { Injectable } from '@angular/core';
import { LoginModel } from '../models/LoginModel';
import { Response, RequestOptions, Headers, URLSearchParams } from "@angular/http";
import { Observable, of, throwError } from 'rxjs';
import { map, catchError, finalize, tap } from 'rxjs/operators'

import { ActivatedRoute } from '@angular/router';
import { AppStore } from '../../../shared/services/store.service';
import { BaseService } from '../../../shared/services/BaseService';
import { HttpClient } from '../../../shared/services/HttpClient';
import { Router } from '@angular/router';
import { ApiResponseModel } from "../../../shared/models/ApiResponseModel";


@Injectable()
export class AuthenticationService extends BaseService {
    constructor(private store: AppStore, private activatedRoute: ActivatedRoute) {
        super();
        this.loadTranslations();
    }

    public loadTranslations() {
        let language = this.storage.get('l') || this.activatedRoute.snapshot.queryParams["l"] || 'en';
        this.storage.save(this.storage.Keys.Languages, language);
    }

    public login(loginModel: LoginModel): Observable<any> {
        return this.anonymousrequest("post", this.apiCallTo("Account/Login"), null, loginModel)
            .pipe(
                map(r => this.getApiResponseModel(r)),
                tap(r => {
                   if( r.data ){
                        this.storeToken(r.data.accessToken);
                   }
                })
            );
    }

    private storeToken(token:string, rememberme?: boolean) {
        this.storage.save(this.storage.Keys.Token, token || null);
       // this.storage.save(this.storage.Keys.RememberMe, rememberme || false);
    }

    public logout() {

        for (var property in this.storage.Keys) {
            if (this.storage.Keys.hasOwnProperty(property) && property !== 'Languages' ) {
                this.storage.delete(this.storage.Keys[property]);
            }
        }

        this.store.initializeStore();
        this.store.set('account.loggedUser', null);
    }
}
