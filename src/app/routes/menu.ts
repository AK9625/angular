
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};

const Organisations = {
    text: 'Organisations',
    link: '/organisations/list',
    icon: 'fas fa-sitemap'
};

const Places = {
    text: 'Places',
    link: '/place/list',
    icon: 'fas fa-store-alt'
};

const Category = {
    text: 'Categories',
    link: '/category/list',
    icon: 'fas fa-store-alt'
};

const Menu = {
    text: 'Menu',
    icon: 'fas fa-utensils',
    submenu: [
    {
        text: 'Menu List',
        link: '/menu/list'
    },
    // {
    //     text: 'Options',
    //     submenu: [
    //     {
    //         text: 'Options List',
    //         link: '/option/list'
    //     },
    //     {
    //         text: 'Propertyes List',
    //         link: '/property/list'
    //     },
    //     ]
    // },
    {
        text: 'Ingredients',
        link: '/ingredient/list'
    },
    ]
};

const Table = {
    text: 'Tables',
    link: '/tables/table-group/list',
    icon: 'fas fa-chair'
};

const Status = {
    text: 'Status',
    link: '/status/list',
    icon: 'fas fa-chair'
};

const Additional = {
    text: 'Additional Info',
    link: '/additional-info/list',
    icon: 'fas fa-chair'
};

const Order = {
    text: 'Orders',
    link: '/order/list',
    icon: 'fas fa-concierge-bell'
};

const Intro = {
    text: 'Intros',
    link: '/intro/list',
    icon: 'fas fa-concierge-bell'
};

const Client = {
    text: 'Clients',
    link: '/client/list',
    icon: 'fas fa-users'
};

const Settings = {
    text: 'Settings',
    link: '/settings/privacy/list',
    icon: 'fas fa-cog'
};

const Notifications = {
    text: 'Notifications',
    icon: 'fas fa-info',
    submenu: [
    {
        text: 'Notification List',
        link: '/notification/list',
    },
    {
        text: 'Notification Role',
        link: '/notification/user-role/list',
    },
    ]
};

const Discount = {
    text: 'Discounts',
    link: '/discount/list',
    icon: 'fas fa-percent',
};

const headingMain = {
    text: 'Main Navigation',
    heading: true
};

const headingComponents = {
    text: 'Components',
    heading: true
};

const headingMore = {
    text: 'More',
    heading: true
};

export const admin = [
    headingMain,
    Order,
    Organisations,
    Places,
    Menu,
    Additional,
    Category,
    Notifications,
    Status,
    Client,
    Intro,
    Settings,
];

export const partner = [
    headingMain,
    Order,
    Organisations,
    Places,
    Menu,
];