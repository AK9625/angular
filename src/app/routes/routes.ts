import { LayoutComponent } from '../layout/layout.component';
//guard
import { RedirectToLoginGuard } from '../shared/services/security/redirectToLoginGuard';
import { RedirectToDashboardGuard } from '../shared/services/security/redirectToDashboardGuard';
import { RedirectToInitializeGuard } from '../shared/services/security/redirectToInitializeGuard';
//component
import { LockComponent } from './pages/lock/lock.component';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { RegisterComponent } from './pages/register/register.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Dashboardv1Component } from './dashboard/dashboardv1/dashboardv1.component';
import { InitializeAppComponent } from '../initialize-app/initialize-app.component';
//module
import { DashboardModule } from './dashboard/dashboard.module';
import { MenuModule } from '../menu.module/menu.module';
import { PlaceModule } from '../place.module/place.module';
import { TableModule } from '../tables.module/tables.module';
import { OrderModule } from '../order.module/order.module';
import { IntroModule } from '../intro.module/intro.module';
import { StatusModule } from '../status.module/status.module';
import { ClientModule } from '../client.module/client.module';
import { OptionModule } from '../option.module/option.module';
import { AccountModule } from '../account.module/account.module';
import { SettingsModule } from '../settings.module/settings.module';
import { CategoryModule } from '../category.module/category.module';
import { PropertyModule } from '../property.module/property.module';
import { DiscountModule } from '../discount.module/discount.module';
import { AdditionalModule } from '../additional.module/additional.module';
import { IngredientModule } from '../ingredient.module/ingredient.module';
import { NotificationModule } from '../notification.module/notification.module';
import { OrganisationModule } from '../organisation.module/organisation.module';

export const routes = [
// {
//     path: '',
//     component: LayoutComponent,
//     children: [
//     { path: '', redirectTo: 'home', pathMatch: 'full' },
//     { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) , canActivate: [RedirectToLoginGuard] },
//     { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
//     { path: 'widgets', loadChildren: () => import('./widgets/widgets.module').then(m => m.WidgetsModule) },
//     { path: 'elements', loadChildren: () => import('./elements/elements.module').then(m => m.ElementsModule) },
//     { path: 'forms', loadChildren: () => import('./forms/forms.module').then(m => m.FormsModule) },
//     { path: 'charts', loadChildren: () => import('./charts/charts.module').then(m => m.ChartsModule) },
//     { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule) },
//     { path: 'maps', loadChildren: () => import('./maps/maps.module').then(m => m.MapsModule) },
//     { path: 'blog', loadChildren: () => import('./blog/blog.module').then(m => m.BlogModule) },
//     { path: 'ecommerce', loadChildren: () => import('./ecommerce/ecommerce.module').then(m => m.EcommerceModule) },
//     { path: 'extras', loadChildren: () => import('./extras/extras.module').then(m => m.ExtrasModule) }
//     ]
// },
{
	path: 'initialize',
	component: InitializeAppComponent,
	canActivate: [RedirectToLoginGuard],
},
{
    path: '',
    component: LayoutComponent,
    canActivate: [RedirectToInitializeGuard, RedirectToLoginGuard],
    children: [
    ...AccountModule.routerRoutes(),
    ...OrganisationModule.routerRoutes(),
    ...DashboardModule.routerRoutes(),
    ...PlaceModule.routerRoutes(),
    ...MenuModule.routerRoutes(),
    ...TableModule.routerRoutes(),
    ...CategoryModule.routerRoutes(),
    ...IngredientModule.routerRoutes(),
    ...AdditionalModule.routerRoutes(),
    ...OptionModule.routerRoutes(),
    ...PropertyModule.routerRoutes(),
    ...StatusModule.routerRoutes(),
    ...OrderModule.routerRoutes(),
    ...IntroModule.routerRoutes(),
    ...ClientModule.routerRoutes(),
    ...DiscountModule.routerRoutes(),
    ...SettingsModule.routerRoutes(),
    ...NotificationModule.routerRoutes(),
    ]
},
// Not lazy-loaded routes
{ path: 'login', component: LoginComponent, canActivate:[RedirectToDashboardGuard] },
{ path: 'logout', component: LogoutComponent },
{ path: 'register', component: RegisterComponent },
{ path: 'recover', component: RecoverComponent },
{ path: 'lock', component: LockComponent },
{ path: 'maintenance', component: MaintenanceComponent },
{ path: '404', component: Error404Component },
{ path: '500', component: Error500Component },

// Not found
{ path: '**', redirectTo: 'order' }

];
