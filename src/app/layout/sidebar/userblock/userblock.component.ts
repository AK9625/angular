import { Component, OnInit } from '@angular/core';
//modele
import { UserProfileModel } from '../../../account.module/models/UserProfileModel';
//service
import { UserblockService } from './userblock.service';
//component
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent extends AuthenticatedComponent implements OnInit {

    user: UserProfileModel = new UserProfileModel();
    picture: string = null;

    constructor(public userblockService: UserblockService) {
        super();
    }

    ngOnInit() {
        this.picture = 'assets/img/user/01.jpg';
        this.user = this.store._("account.loggedUser");
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}
