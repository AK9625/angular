import { Component, OnInit } from '@angular/core';
//models
import { NotificationModel } from '../../models/NotificationModel';
//services
import { NotificationService } from '../../services/notfication.service';
import { NotificationRoleService } from '../../services/notfication.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'notification-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class NotificationListComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private noti: NotificationRoleService,
		private notService: NotificationService) {
		super()
	}

	public notList: Array<NotificationModel> = [];
	public data: Array<NotificationModel> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'name', name: 'name' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnInit() {
		this.notService.getAll().subscribe(data => {
			this.notList = data;
		});
	}

	public save(not: NotificationModel ) {

		this.noti.roleToggle(not.id).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Notification updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}
