import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { NotificationModel } from '../models/NotificationModel';

@Injectable()
export class NotificationService extends CrudService<NotificationModel> {

    protected serviceItemsKey = 'notification';

    constructor(protected store: AppStore) {
        super('Notification', null);
    }

    public preloadData() {
        return [
            this.getAllNotifications(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllNotifications() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }
}

@Injectable()
export class NotificationRoleService extends CrudService<NotificationRoleRef> {

    protected serviceItemsKey = 'NotificationRoleRef';

    constructor(protected store: AppStore) {
        super('NotificationRoleRef', null);
    }

    public preloadData() {
        return [
            this.getAllNotificationRoleRef(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllNotificationRoleRef() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                // tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }    

    public roleToggle(id) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/toggle?notificationRoleRefId=${id}`))
            .pipe(
                map(i => i.data),
                // tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

}

@Injectable()
export class RoleService extends CrudService<RoleModel> {

    protected serviceItemsKey = 'Role';

    constructor(protected store: AppStore) {
        super('Role', null);
    }

    public preloadData() {
        return [
            this.getRoles(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getRoles() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
            );
    }

    public getAllPersonalForPlace(id) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/GetAllPersonalForPlace?placeId=${id}`))
            .pipe(
                map(i => i.data),
            );
    }

}

export class NotificationRoleRef {
  id:number ;
  roleId: number ;
  notificationIds: Array<number> = [];
  isActive: boolean = true;
}

export class RoleModel{
    id: number;
    name: string;
    placeId: number;
    place: any;
    isActive: boolean = true;
}