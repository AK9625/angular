export class NotificationModel {
	id: number;
	title: string;
	body: string;
	type: number;
	sound: string;
	priority: number;
	titleTranslationId: number = 0; 
	bodyTranslationId: number = 0;
	isActive: boolean = true;
}