import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//services
import { RoleService } from "../../services/notfication.service";
import { PlaceService } from "../../../place.module/services/place.service";
//components
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';

@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class RoleEditComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private activeRoute: ActivatedRoute,
		private placeService: PlaceService,
		private roleService: RoleService) {
		super();
	}

	role: RoleModel = new RoleModel();
	place: any = null;

	ngOnInit() {

		let placeId = this.activeRoute.snapshot.params['placeId'];

		if (!!placeId) {
			this.role.placeId = Number(placeId);
			this.placeService.get(placeId).subscribe(res => {
				this.place = res.name;
			});
		}
		if (this.isEdit) {
			let id = this.activeRoute.snapshot.params['id'];
			this.roleService.get(id).subscribe(res => {
				if (!!res) {
					this.role = res;
					this.placeService.get(res.placeId).subscribe(res => {
						this.place = res.name;
					});
				}else {
					this.alertService.showError('Something went wrong!');
					this.back();
				}
			});
		}
	}	

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid.');
			return false;
		}
		return true;
	}

	public save() {

		if (!this.isFormValid()) return;

		delete this.role.place;

		if (!this.role.id) {

			this.roleService.save(this.role).subscribe(data => {

				if(!data.hasError) {
					this.alertService.showSuccess('Role add successfully.');
						this.back();
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});
		} else {
			this.roleService.save(this.role).subscribe(data => {

				if(!data.hasError) {
					this.alertService.showSuccess('Role updated successfully.');
					this.back();
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});			
		}
	}

}

export class RoleModel{
	id: number;
	name: string;
	placeId: number;
	place: any;
	isActive: boolean = true;
}