import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class NotificationStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('notification', null);
        this.set('NotificationRoleRef', null);
    }
}