import { Component, OnInit } from '@angular/core';
//models
import { NotificationModel } from '../../models/NotificationModel';
//services
import { NotificationRoleService } from '../../services/notfication.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class NotificationRoleListComponent extends AuthenticatedComponent implements OnInit {


	constructor(
		private notService: NotificationRoleService) {
		super()
	}

	public notList: Array<any> = [];
	public data: Array<NotificationModel> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'title', name: 'title' },
	{ title: 'name', name: 'Role' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnInit() {
		this.notService.getAll().subscribe(data => {
			this.notList = data;
		});
	}

	public save(not) {

		this.notService.roleToggle(not).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Notification updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}

}
