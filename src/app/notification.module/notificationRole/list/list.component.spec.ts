import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationRoleListComponent } from './list.component';

describe('NotificationRoleListComponent', () => {
  let component: NotificationRoleListComponent;
  let fixture: ComponentFixture<NotificationRoleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationRoleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationRoleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
