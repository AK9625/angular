import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { KeyValuePair } from '../../../models/KeyValuePair';
import { CategoryModel } from '../../models/CategoryModel';
import { CategoryInsertModel } from '../../models/CategoryModel';
//services
import { CategoryService } from "../../services/category.service";
//components
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class CategoryEditComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private activeRoute: ActivatedRoute,
		private categoryService: CategoryService) {
		super();
	}

	public file: any = null;
	public category: CategoryModel = new CategoryModel();

	ngOnInit() {

		let parentId = this.activeRoute.snapshot.params['parentId'];

		if (!!parentId) {
			this.category.parentId = Number(parentId);
		}

		if (this.isEdit) {
			let categoryId = this.activeRoute.snapshot.params['id'];
			this.categoryService.get(categoryId).subscribe(res => {
				if (!!res) {
					this.category = res;
				}else {
					this.alertService.showError('Something went wrong!');
					this.back();
				}
			});
		}
	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid.');
			return false;
		}
		return true;
	}

	// public back() {
	// 	this.router.navigateByUrl("category/list");
	// }

	public createInsertModel(category: CategoryModel): CategoryInsertModel {

		let insertModel: CategoryInsertModel = new CategoryInsertModel();

		insertModel.id = category.id;
		insertModel.name = category.name;
		insertModel.parentId = category.parentId;
		insertModel.description = category.description;
		insertModel.imageUrl = category.imageUrl;
		insertModel.nameTranslationId = category.nameTranslationId;
		insertModel.descriptionTranslationId = category.descriptionTranslationId;
		insertModel.isActive = category.isActive;

		return insertModel;
	}

	public save() {

		if (!this.isFormValid()) return;

		let insertModel = this.createInsertModel(this.category);

		if (!this.category.id) {

			if (this.file) {

				this.categoryService.multipleUploadFile(this.file).subscribe(res => {

					let error = res.errors.map(r => {return r.message});
					this.alertService.showError(error);
					insertModel.imageUrl = res.data[0].url;

					this.categoryService.save(insertModel).subscribe(data => {

						if(!data.hasError) {
							this.alertService.showSuccess('Category add successfully.');
							this.categoryService.loadData().subscribe(res => {
								this.back();
							})
						} else {
							let errors = data.errors.map(r => {return r.message});
							this.alertService.showError(errors);
						}
					});
				});
			}else {
				this.alertService.showError('Category image is required!');

			}	
		} else {

			if (this.file) {
				this.categoryService.multipleUploadFile(this.file).subscribe(res => {

					let error = res.errors.map(r => {return r.message});
					this.alertService.showError(error);
					insertModel.imageUrl = res.data[0].url;

					this.categoryService.save(insertModel).subscribe(data => {

						if(!data.hasError) {

							this.alertService.showSuccess('Category updated successfully.');
							this.categoryService.loadData().subscribe(res => {
								this.back();
							})

						} else {
							let errors = data.errors.map(r => {return r.message});
							this.alertService.showError(errors);
						}
					});
				});

			} else {

				this.categoryService.save(insertModel).subscribe(data => {
					if(!data.hasError) {
						this.alertService.showSuccess('Category updated successfully.');
						this.categoryService.loadData().subscribe(res => {
							this.back();
						})
					} else {
						let errors = data.errors.map(r => {return r.message});
						this.alertService.showError(errors);
					}
				});
			}	
		}
	}
}