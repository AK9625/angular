import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class CategoryStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('category', null);
    }
}