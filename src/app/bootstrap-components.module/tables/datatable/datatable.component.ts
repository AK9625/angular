import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectorRef } from "@angular/core";
import * as _ from 'lodash';

import { TableData } from './ng2-table-data';

@Component({
    selector: 'app-datatable',
    templateUrl: './datatable.component.html',
    styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit {

    constructor(private cdr: ChangeDetectorRef) {
    }

    @Input() color: string = "primary";
    @Input() fakeLoadingFor: number = 0;
    @Input() emptyData: boolean = true;
    @Input() emptyDataText: string = null;
    @Input() settings: boolean = false;
    @Output() tableChange = new EventEmitter();

    public singleData;
    public rows: Array<any> = [];
    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;
    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-striped', 'table-bordered', 'mb-0', 'd-table-fixed']
    };

    private _data;
    set data(value) {
        this._data = value || [];
    }
    @Input() get data() {
        return this._data;
    }

    private _columns;
    set columns(value) {
        this._columns = value;
    }
    @Input() get columns() {
        return this._columns;
    }
    private _fakeLoading: boolean = false;
    public get fakeLoading(): boolean {
    return this._fakeLoading;
    }
    public set fakeLoading(v: boolean) {
    this._fakeLoading = v;
    }

    handlefakeLoading() {
        if (this.fakeLoadingFor <= 0) return;

        this.fakeLoading = true;
        const fakeTimeout = setTimeout(() => {
          this.fakeLoading = false;
          this.cdr.markForCheck();
            this.onChangeTable(this.config);
        this.length = this.data.length;

          clearTimeout(fakeTimeout);
        }, this.fakeLoadingFor);
    }

    public ngOnInit(): void {

    }

    public ngOnChanges(): void { 
        this.handlefakeLoading();
    }

    public changePage(page: any, data: Array<any> = this.data): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any, config: any): any {

        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name] && item[column.name].toString().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            (<any>Object).assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            (<any>Object).assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.data, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.tableChange.emit(this.rows);
        this.length = sortedData.length;
    }

    public onCellClick(data: any): any {
    }

}
