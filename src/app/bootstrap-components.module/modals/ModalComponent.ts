import { Subject } from "rxjs";

export class ModalComponent {
    public actionResponse: Subject<any>;
    constructor() {
        this.actionResponse = new Subject();
    }
}