import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";

@Injectable()
export class OptionStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('option', null);
    }
}