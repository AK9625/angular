import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { OptionModel } from '../../models/OptionModel';
//services
import { OptionService } from '../../services/option.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
  selector: 'app-option-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class OptionListComponent extends AuthenticatedComponent implements OnChanges {

	constructor(
		private activeRoute: ActivatedRoute,
		private optionService: OptionService) {
		super();
	}

	@Input() menuItemId:number = null;
	public optionList: Array<OptionModel> = [];
	public data: Array<OptionModel> = [];
	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'name', name: 'name' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnChanges() {
		this.optionService.getAllByMenuItem(this.menuItemId).subscribe(data => {
			this.optionList = data;
		});
	}

	public save(option: OptionModel ) {
		this.optionService.save(option).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Option updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}
