import { Component, OnInit, Input , OnChanges} from '@angular/core';
//models
import { TableGroupModel } from '../../models/TableGroupModel';
//services
import { TablesGroupesService } from '../../services/tableGroupes.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
  selector: 'table-groupes-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class TableGropesListComponent extends AuthenticatedComponent implements OnChanges {

	constructor(
		private tgroupesService: TablesGroupesService) {
		super();
	}

	@Input() placeId: number = null;
	@Input() tablegroups: Array<TableGroupModel> = [];	
	public tableGroupList: Array<TableGroupModel> = [];
	public data: Array<TableGroupModel> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'name', name: 'name' },
	{ title: 'place', name: 'place' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnChanges() {
		if (!this.placeId) {
			this.tgroupesService.getAll().subscribe(data => {
				this.tableGroupList = data;
			});
		}
	}

	public save(table: TableGroupModel ) {
		this.tgroupesService.save(table).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Table Group updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}
