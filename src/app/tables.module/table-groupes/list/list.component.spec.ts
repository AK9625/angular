import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableGropesListComponent } from './list.component';

describe('TableGropesListComponent', () => {
  let component: TableGropesListComponent;
  let fixture: ComponentFixture<TableGropesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableGropesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableGropesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
