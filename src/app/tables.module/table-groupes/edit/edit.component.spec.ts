import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableGropesEditComponent } from './edit.component';

describe('TableGropesEditComponent', () => {
  let component: TableGropesEditComponent;
  let fixture: ComponentFixture<TableGropesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableGropesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableGropesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
