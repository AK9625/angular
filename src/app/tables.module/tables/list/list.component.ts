import { Component, OnInit, Input , OnChanges } from '@angular/core';
//models
import { TableModel } from '../../models/TableModel';
//services
import { TablesService } from '../../services/tables.service';
//componts
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'tables-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class TableListComponent extends AuthenticatedComponent implements OnChanges {

	constructor(
		private tablesService: TablesService) {
		super();
	}

	@Input() placeId: number = null;
	@Input() tables: Array<TableModel> = [];
	public tableList: Array<TableModel> = [];
	public data: Array<TableModel> = [];

	public columns: Array<any> = [

				{ title: 'id', name: 'id' },
				{ title: 'name', name: 'name' },
				{ title: 'number', name: 'number' },
				{ title: 'table Group', name: 'table Group' },
				{ title: 'settings', name: 'settings' },

				];


	ngOnInit() {}
	ngOnChanges() {

		if (!this.placeId) {

			this.tablesService.getAll().subscribe(data => {
				this.tableList = data;
			});
		}
	}

	public save(table: TableModel ) {
		this.tablesService.save(table).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Table updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}