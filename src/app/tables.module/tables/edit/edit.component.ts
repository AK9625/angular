import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { TableModel } from '../../models/TableModel';
import { KeyValuePair } from '../../../models/KeyValuePair';
//services
import { TablesService } from "../../services/tables.service";
import { PlaceService } from "../../../place.module/services/place.service";
import { TablesGroupesService } from "../../services/tableGroupes.service";
//components
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class TableEditComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private placeService: PlaceService,
		private activeRoute: ActivatedRoute,
		private tablesService: TablesService,
		private tgroupService: TablesGroupesService) {
		super();
	}

	@ViewChild('pRef', {static: false}) pRef

	public table: TableModel = new TableModel();
	public placeList: Array<KeyValuePair> = [];
	public tableGroupList: Array<KeyValuePair> = [];
	public elementType: 'url' | 'canvas' | 'img' = 'url';
	public href: string = '';
	public placeId: string = null;
	public typelist: Array<KeyValuePair> = [

	new KeyValuePair(1, 'Restourant'),
	new KeyValuePair(0, 'Fast Food'),

	];	

	ngOnInit() {

		this.placeId = this.activeRoute.snapshot.params['placeId'];

		if ( !!this.placeId ) {
			this.table.placeId = Number(this.placeId);

			this.tgroupService.getAllForPlace(this.table.placeId).subscribe(res => {
				this.tableGroupList = res.map(k => new KeyValuePair(k.id, k.name));
			});
		}

		this.placeService.getAllSimplified().subscribe(res => {
			this.placeList = res.map(k => new KeyValuePair(k.id, k.name));
		});


		if (this.isEdit) {
			let tableId = this.activeRoute.snapshot.params['id'];
			this.tablesService.get(tableId).subscribe(res => {
				if (!!res) {
					this.table = res;
				}else {
					this.alertService.showError('Something went wrong!');
					this.back();
				}
			});
		}
	}

	public getTableGroupsByPlace() {

		if(!!this.table.placeId) {

			this.tgroupService.getAllForPlace(this.table.placeId).subscribe(res => {
				this.tableGroupList = res.map(k => new KeyValuePair(k.id, k.name));
			})
		}
	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid.');
			return false;
		}
		return true;
	}

	public save() {

		if (!this.isFormValid()) return;

		this.table.tableGroupId = (this.table.tableGroupId != 0) ? this.table.tableGroupId : undefined ;  

		if (!this.table.id ) {

			this.tablesService.save(this.table).subscribe(data => {
				if(!data.hasError) {
					this.alertService.showSuccess('Table add successfully.');
					this.tablesService.updateData({id:data.data}).subscribe(res => {
						//this.placeService.loadData().subscribe();
						this.back();
					})
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});	

		} else {

			this.table.place = null;
			this.table.tableGroup = null;
			this.tablesService.save(this.table).subscribe(data => {
				if(!data.hasError) {
					this.alertService.showSuccess('Table updated successfully.');
					this.tablesService.updateData({id:this.table.id}).subscribe(res => {
						//this.placeService.loadData().subscribe();
						this.back();
					})
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			});
		}
	}

	public getUrl() {
		this.href = this.pRef.qrcElement.nativeElement.firstChild.src;
	}
}
