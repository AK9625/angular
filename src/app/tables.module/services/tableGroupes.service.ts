import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { TableGroupModel } from '../models/TableGroupModel';

@Injectable()
export class TablesGroupesService extends CrudService<TableGroupModel> {

    protected serviceItemsKey = 'tablegroup';

    constructor(protected store: AppStore) {
        super('TableGroup', null);
    }

    public preloadData() {
        return [
            this.getAllTableGroupes(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllForPlace (id:number ) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/GetAllForPlace?placeId=${id}`))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

    public getAllTableGroupes() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

}