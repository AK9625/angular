export class TableGroupModel{
	id: number;
	name: string;
	placeId: number;
	place: any ;
	isActive: boolean = true;
}