import { MenuModel } from './MenuModel';

export class SectionModel{
	id: number;
	name: string;
	menuId: number;
	roleIds: Array<number> = [];
	menu: MenuModel = new MenuModel();
	role: RoleModel = new RoleModel();
	nameTranslationId: number = 0;
	menuItems: any = [];
	isActive: boolean = true;
}

export class SectionInsertModel{
	id: number;
	name: string;
	menuId: number;
	roleIds: Array<number> = [];
	nameTranslationId: number = 0;
	isActive: boolean = true;
}

export class RoleModel{
	id: number;
	name: string;
	active:	boolean;
	sections: any = [];
	isActive: boolean = true;
}