import { ImageModel } from '../../models/ImageModel';
import { PlaceModel } from '../../place.module/models/PlaceModel';

export class MenuModel{
	id: number;
	name: string;
	placeId: number;
	imageUpload: ImageUploadModel = new ImageUploadModel();
	imageIds: Array<number> = [];
	place: PlaceModel ;
	imageUrls: Array<ImageModel> = [];
	sections: any;
	nameTranslationId: number = 0;
	isActive: boolean = true;
}

export class MenuInsertModel {
	id: number;
	name: string;
	placeId: number;
	imageIds: Array<number> = [];
	nameTranslationId: number = 0;
	isActive: boolean = true;
}

export class ImageUploadModel {
	id: number;
	url: string;
}