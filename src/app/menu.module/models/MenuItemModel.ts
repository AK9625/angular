import { MenuModel } from './MenuModel';
import { ImageModel } from '../../models/ImageModel';
import { SectionModel } from './SectionModel';

export class MenuItemModel{
	id: number;
	name: string;
	menuId: number;
	price: number;
	weight: number;
	pieces: number;
	calories: number;
	description: string;
	sectionId: number;
	averageCount: number;
	averageRate: number;
	section:SectionModel = new SectionModel();
	menu:MenuModel = new MenuModel();
	imageIds: Array<number> = [];
	ingredientIds: Array<number> = [];
	imageUrl: string;
	options: Array<OptionModel>;
	ingredients: Array<IngredientModel>;
	nameTranslationId: number = 0;
	descriptionTranslationId: number = 0;
	isActive: boolean = true;
}

export class MenuItemInsertModel{
	id: number;
	name: string;
	menuId: number;
	price: number;
	weight: number;
	pieces: number;
	calories: number;
	sectionId: number;
	description: string;
	averageCount: number;
	averageRate: number;
	imageIds: Array<number>;
	ingredientIds: Array<number>;
	nameTranslationId: number = 0;
	descriptionTranslationId: number = 0;
	isActive: boolean = true;
}

export class OptionModel {
	id: number;
	title: string;
	menuItemId: number;
	description: string;
	type: number;
	menuItem: any;
	properties: any;
	titleTranslationId: number = 0;
	descriptionTranslationId: number = 0;
	isActive: boolean = true;
}

export class IngredientModel {
	id: number;
	name: string;
	price: number;
	calories: number;
	type: number;
	nameTranslationId: number;
	isActive: boolean = true;
}