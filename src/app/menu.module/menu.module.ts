import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "../shared/shared.module";
import { RedirectToLoginGuard } from '../shared/services/security/redirectToLoginGuard';
import { DataStore } from '../shared/models/DataStore';
import { MenuStore } from './menu.store';
import { appInjector } from '../bootstrap-components.module/utils/appInjector';
import { AppStore } from '../shared/services/store.service';
import { routes } from './menu.routes';
import { AgmCoreModule } from '@agm/core';
import { NgxSelectModule, INgxSelectOptions } from 'ngx-select-ex'

const CustomSelectOptions: any = { // Check the interface for more options
    optionValueField: 'id',
    optionTextField: 'name'
};


import { OptionModule } from '../option.module/option.module';
import { MenuService } from './services/menu.service'
import { SectionService } from './services/menu.service'
import { MenuItemService } from './services/menu.service'
import { MenuShellComponent } from './menu-shell.component';
import { MenuListComponent } from './menu/list/list.component';
import { MenuEditComponent } from './menu/edit/edit.component';
import { MenuItemComponent } from './menu/edit/menu-item/menu-item.component';
import { SectionsComponent } from './menu/edit/sections/sections.component';
import { MenuComponent } from './menu/edit/menu/menu.component';
import { SectionModalComponent } from './menu/edit/sections/section-modal/section-modal.component';
import { MenuItemModalComponent } from './menu/edit/menu-item/menu-item-modal/menu-item-modal.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    SharedModule,
    RouterModule,
    NgxSelectModule.forRoot(CustomSelectOptions),
    OptionModule,

  ],
  declarations: [
    MenuShellComponent,
    MenuListComponent,
    MenuEditComponent,
    MenuItemComponent,
    SectionsComponent,
    MenuComponent,
    SectionModalComponent,
    MenuItemModalComponent,
  ],

  exports: [
    MenuListComponent,
  ],
  entryComponents: [SectionModalComponent, MenuItemModalComponent],

  providers: [MenuService, SectionService, MenuItemService]
})
export class MenuModule {

  static preloadData() {
    const services = this.moduleServices();
    return {
      resources: [
        ...services.menuService.preloadData(),
      ]
    };
  }

  private static moduleServices() {
    let appinjector = appInjector.injector();
    let store = appinjector.get(AppStore);
    let menuService = appinjector.get(MenuService);

    return {
      store,
      menuService,
    }
  }

  static initializeModuleData() {
    let injector = appInjector.injector();
    let store = injector.get(AppStore);
    MenuStore.prototype.initializeStore.call(store);
    this.loadData();
  }

  static routerRoutes() {
    return routes;
  }

  static initializeStore(store) {
    MenuStore.prototype.initializeStore.call(store);
  }

  public static loadData() {
    const services = this.moduleServices();
    services.menuService.preloadData();
  }

}
