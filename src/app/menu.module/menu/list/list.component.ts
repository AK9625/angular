import { Component, OnInit, Input } from '@angular/core';
//services
import { MenuService } from "../../services/menu.service";
//models
import { MenuModel } from '../../models/MenuModel';
import { MenuInsertModel } from '../../models/MenuModel';
//componts
import { BaseComponent } from '../../../shared/components/BaseComponent';
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'menu-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class MenuListComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private menuService: MenuService) {
		super();
	}

	@Input() placeId: number = null;
	@Input() menus: Array<MenuModel> = [];
	public menuList: Array<MenuModel> = [];
	public data: Array<MenuModel> = [];

	public columns: Array<any> = [

	{ title: 'id', name: 'id' },
	{ title: 'logo', name: 'logo' },
	{ title: 'name', name: 'name' },
	{ title: 'settings', name: 'settings' },

	];

	ngOnInit() {
		if (!this.placeId) {
			this.menuService.getAll().subscribe(data => {
				this.menuList = data;
			});
		}
	}

	createInsertModel(menu: MenuModel): MenuInsertModel {

		let insertModel: MenuInsertModel = new MenuInsertModel();

		insertModel.id = menu.id;
		insertModel.name = menu.name;
		insertModel.placeId = menu.placeId;
		insertModel.imageIds = menu.imageUrls.map(item => item.id);
		insertModel.nameTranslationId = menu.nameTranslationId;
		insertModel.isActive = menu.isActive;

		return insertModel;
	}

	public save(menu: MenuModel ) {

		let insertModel = this.createInsertModel(menu);

		this.menuService.save(insertModel).subscribe(data => {		
			if(!data.hasError ) {
				this.alertService.showSuccess('Menu updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}