import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { MenuModel } from '../../../models/MenuModel';
import { SectionModel } from '../../../models/SectionModel';
import { SectionInsertModel } from '../../../models/SectionModel';
//services
import { SectionService } from "../../../services/menu.service";
import { ModalsService } from '../../../../bootstrap-components.module/modals/modals.service';
//components
import { AuthenticatedComponent } from '../../../../shared/components/AuthenticatedComponent';
import { SectionModalComponent } from './section-modal/section-modal.component';
@Component({
	selector: 'app-sections',
	templateUrl: './sections.component.html',
	styleUrls: ['./sections.component.scss']
})
export class SectionsComponent extends AuthenticatedComponent implements OnInit, OnChanges {

	constructor(
		private activeRoute: ActivatedRoute,
		private modalService: ModalsService,
		private sectionService: SectionService) {
		super();
	}

	private modalRef: any = null;
	public sectionList: Array<SectionModel> = [];
	public data: Array<SectionModel> = [];

	@Input() menu: SectionModel = new SectionModel();

	public columns: Array<any> = [
	{ title: 'id', name: 'id' },
	{ title: 'name', name: 'name' },
	{ title: 'settings', name: 'settings' },
	];

	ngOnInit() {
	}

	ngOnChanges() {
		this.store.$("section").subscribe(c => {
			setTimeout(() => {
				this.sectionList = c;
			}, 50);
			this.hideModal();
		})

		if (this.menu.id ) {
			this.sectionService.getAllSections(this.menu.id).subscribe(data => {
				this.sectionList = data;
			});
		}
	}

	public addSection(id:number = null) {
		this.modalRef = this.modalService.simpleLg(!!id ? 'Update Section' : 'Add Section', SectionModalComponent, {
			menu: this.menu,
			sectionId: id
		});
	}

	public hideModal() {
		if (this.modalRef) {
			setTimeout(() => {
				this.modalRef.ref.hide();
			}, 10);
		}
	}

	createInsertModel(section: SectionModel): SectionInsertModel {

		let insertModel: SectionInsertModel = new SectionInsertModel();

		insertModel.id = section.id;
		insertModel.name = section.name;
		insertModel.menuId = section.menuId;
		insertModel.roleIds = section.roleIds;
		insertModel.nameTranslationId = section.nameTranslationId;
		insertModel.isActive = section.isActive;

		return insertModel;
	}

	public save(section: SectionModel ) {

		let insertModel = this.createInsertModel(section);

		this.sectionService.save(insertModel).subscribe(data => {
			if(!data.hasError ) {
				this.alertService.showSuccess('Section updated successfully.');
			} else {
				let errors = data.errors.map(r => {return r.message});
				this.alertService.showError(errors);
			}
		})
	}
}
