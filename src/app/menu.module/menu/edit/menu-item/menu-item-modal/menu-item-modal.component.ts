	import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FormControl} from '@angular/forms';
//models
import { MenuModel } from '../../../../models/MenuModel';
import { SectionModel } from '../../../../models/SectionModel';
import { MenuItemModel } from '../../../../models/MenuItemModel';
import { KeyValuePair } from '../../../../../models/KeyValuePair';
import { IngredientModel } from '../../../../models/MenuItemModel';
import { MenuItemInsertModel } from '../../../../models/MenuItemModel';

//services
import { SectionService } from "../../../../services/menu.service";
import { MenuItemService } from "../../../../services/menu.service";
import { IngredientService } from "../../../../../ingredient.module/services/ingredient.service";
import { ModalsService } from '../../../../../bootstrap-components.module/modals/modals.service';
//components
import { AuthenticatedComponent } from '../../../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-menu-item-modal',
	templateUrl: './menu-item-modal.component.html',
	styleUrls: ['./menu-item-modal.component.scss']
})
export class MenuItemModalComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private activeRoute: ActivatedRoute,
		private menuItemService: MenuItemService,
		private ingredientService: IngredientService,
		private sectionService: SectionService) {
		super();
	}

	file: any = null;
	formId: string = 'menu-item-form';
	ingredientList: Array<any> = [];
	sectionList: Array<KeyValuePair> = [];
	menuItemId: number = null;
	menuItem: MenuItemModel = new MenuItemModel();
	public selectControl = new FormControl();

	ngOnInit() {
		this.menuItemId = this.activeRoute.snapshot.params['id'];

		if (!!this.isEdit) {
			this.menuItemService.get(this.menuItemId).subscribe(res => {
				if (!!res) {
					this.menuItem = res;
				}else {
					this.alertService.showError('Something went wrong!');
				}
			});

		} else {
			this.menuItem.menuId = this.activeRoute.snapshot.params['menuId'];
		}
		this.sectionService.getAllSections(this.menuItem.menuId).subscribe(res => {
			this.sectionList = res.map(k => new KeyValuePair(k.id, k.name));
		});

		this.ingredientService.getAll().subscribe(res => {
			this.ingredientList = res;
		});

		// this.selectControl.valueChanges.subscribe(value => {this.menuItem.ingredientIds = value});
	}

	delete(url) {
		this.menuItemService.deleteFile(url).subscribe(res => {
			if (res) {
				this.menuItemService.updateData({id:this.menuItem.id}).subscribe(res => {
					this.menuItem = res.data;
					this.alertService.showSuccess('The image is Deleted!');
				});
			}
		})
	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public createInsertModel(menuItem: MenuItemModel): MenuItemInsertModel {

		let insertModel: MenuItemInsertModel = new MenuItemInsertModel();
		insertModel.id = menuItem.id;
		insertModel.name = menuItem.name;
		insertModel.menuId = menuItem.menuId;
		insertModel.price = menuItem.price;
		insertModel.weight = menuItem.weight;
		insertModel.pieces = menuItem.pieces;
		insertModel.imageIds = menuItem.imageIds;
		insertModel.calories = menuItem.calories;
		insertModel.sectionId = menuItem.sectionId;
		insertModel.averageRate = menuItem.averageRate;
		insertModel.averageCount = menuItem.averageCount;
		insertModel.description = menuItem.description;
		insertModel.ingredientIds = menuItem.ingredientIds;
		insertModel.nameTranslationId = menuItem.nameTranslationId;
		insertModel.descriptionTranslationId = menuItem.descriptionTranslationId;
		insertModel.isActive = menuItem.isActive;

		return insertModel;
	}

	public imagesUpload():Promise<Array<number>> {
		return new Promise((resolve, reject) => {
			if (this.file) {
				this.menuItemService.multipleUploadFile(this.file).subscribe(res => {
					resolve([...res.data.map(item => { return item.id })]);
				});
			} else {
				resolve(); 
			}
		});
	}

	public save() {

		if (!this.isFormValid()) return;

		let insertModel = this.createInsertModel(this.menuItem);

		if (!this.menuItem.id) {

			this.imagesUpload().then(imageIds => {

				if (!!imageIds) { insertModel.imageIds = [ ...imageIds] }

				if (insertModel.imageIds.length > 0 ) {

					this.menuItemService.save(insertModel).subscribe(d => {
						if (!d.hasError) {
							this.menuItemService.updateData({id:d.data}).subscribe(res => {
								this.alertService.showSuccess('Menu Item added successfully.');
								this.back();
							})
						} else {
							let errors = d.errors.map(r => {return r.message});
							this.alertService.showError(errors);
						}
					});
				} else {
                    this.alertService.showError('Menu Item Image is required!');
				}
			})
		} else {

			this.imagesUpload().then(imageIds => {

				if (!!imageIds) { insertModel.imageIds = [ ...imageIds] }

				// if (insertModel.imageIds.length > 0 ) {

					this.menuItemService.save(insertModel).subscribe(d => {

						if (!d.hasError) {
							this.menuItemService.updateData({id:insertModel.id}).subscribe(res => {
								this.alertService.showSuccess('Menu Item updated successfully.');
								this.back();
							})
						} else {
							let errors = d.errors.map(r => {return r.message});
							this.alertService.showError(errors);
						}
					});
				// } else {
    //                 this.alertService.showError('Menu Item Image is required!');
				// }
			})
		}
	}

	public isFormValid() {
		if (!super.isFormValid()) {
			this.alertService.showError("The form is not valid.");
			return false;
		}
		return true;
	}

	public getIngredients(event) {
		this.menuItem.ingredientIds = event.map(i => i.key);
	}

}
