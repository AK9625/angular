import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { MenuModel } from '../../../models/MenuModel';
import { MenuInsertModel } from '../../../models/MenuModel';
import { KeyValuePair } from '../../../../models/KeyValuePair';
//services
import { MenuService } from "../../../services/menu.service";
import { PlaceService } from "../../../../place.module/services/place.service";
//components
import { AuthenticatedComponent } from '../../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss']
})
export class MenuComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		private menuService: MenuService,
		private placeService: PlaceService,
		private activeRoute: ActivatedRoute) {
		super();
	}

	file: any = null;
	@Input() menu: MenuModel = new MenuModel();
	placeList: Array<KeyValuePair> = [];

	ngOnInit() {
		this.placeService.getAllSimplified().subscribe(res => {
			this.placeList = res.map(k => new KeyValuePair(k.id, k.name));
		});
	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public isFormValid() {

		if (!super.isFormValid()) {
			this.alertService.showError('The form is not valid');
			return false;
		}
		return true;
	}

	// public back() {
	// 	this.router.navigateByUrl("menu/list");
	// }

	delete(url) {
		this.menuService.deleteFile(url).subscribe(res=> {
			if (res) {
				this.menuService.updateData({id:this.menu.id}).subscribe(res => {
					this.menu = res.data;
					this.alertService.showSuccess('The image is Deleted!');
				});
			}
		})
	}

	createInsertModel(menu: MenuModel): MenuInsertModel {

		let insertModel: MenuInsertModel = new MenuInsertModel();

		insertModel.id = menu.id;
		insertModel.name = menu.name;
		insertModel.placeId = menu.placeId;
		insertModel.imageIds = menu.imageUrls.map(item=> item.id);
		insertModel.nameTranslationId = menu.nameTranslationId;
		insertModel.isActive = menu.isActive;

		return insertModel;
	}

	public save() {

		if (!this.isFormValid()) return;

		let insertModel = this.createInsertModel(this.menu);

		if (!this.menu.id) {

			if (!!this.file) {

				this.menuService.multipleUploadFile(this.file).subscribe(res => {

					let error = res.errors.map(r => {return r.message});
					this.alertService.showError(error);
					insertModel.imageIds.push(res.data[0].id);

					this.menuService.save(insertModel).subscribe(data => {

						if(!data.hasError) {

							this.alertService.showSuccess('Menu added successfully.');
							this.menuService.updateData({id:data.data}).subscribe(res => {
								this.back();
							})

						} else {
							let errors = data.errors.map(r => {return r.message});
							this.alertService.showError(errors);
						}
					});
				});
			} else {
				this.alertService.showError('Menu Image Fild is required!');
			}

		} else {

			if (this.file) {
				this.menuService.multipleUploadFile(this.file).subscribe(res => {

					let error = res.errors.map(r => {return r.message});
					this.alertService.showError(error);
					insertModel.imageIds.push(res.data[0].id);

					this.menuService.save(insertModel).subscribe(data => {

						if(!data.hasError) {

							this.alertService.showSuccess('Menu updated successfully.');
							this.menuService.updateData({id:insertModel.id}).subscribe(res => {
								this.placeService.loadData().subscribe();
								this.back();
							})

						} else {
							let errors = data.errors.map(r => {return r.message});
							this.alertService.showError(errors);
						}
					});
				});

			} else {

				this.menuService.save(insertModel).subscribe(data => {
					if(!data.hasError) {
						this.alertService.showSuccess('Menu updated successfully.');
						this.menuService.updateData({id:data.data}).subscribe(res => {
							this.placeService.loadData().subscribe();
							this.back();
						})
					} else {
						let errors = data.errors.map(r => {return r.message});
						this.alertService.showError(errors);
					}
				});
			}
		}
	}
}