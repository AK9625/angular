import { Injectable } from '@angular/core';
import { CrudService } from './../../shared/services/crud.service';
import { AppStore } from './../../shared/services/store.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { MenuModel } from '../models/MenuModel';
import { SectionModel } from '../models/SectionModel';
import { MenuItemModel } from '../models/MenuItemModel';

@Injectable()
export class MenuService extends CrudService<MenuModel> {
    protected serviceItemsKey = 'menu';

    constructor(protected store: AppStore) {
        super('Menu', null);
    }

    public preloadData() {
        return [
            this.getAllMenues(),
        ];
    }

    public loadData() {
        return forkJoin(this.preloadData());
    }

    public getAllMenues() {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + '/GetAll'))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }
}

@Injectable()
export class SectionService extends CrudService<SectionModel> {
    protected serviceItemsKey = 'section';

    constructor(protected store: AppStore) {
        super('Section', null);
    }

    public getAllSections(id: number) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/GetAll?menuId=${id}`))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

    public getAllRoles() {
        return this.apirequest('get', this.apiCallTo('Role/GetAll'))
            .pipe(
                map(i => i.data),
            );
    }
    public getAllPersonal() {
        return this.apirequest('get', this.apiCallTo('Role/GetAllPersonal'))
            .pipe(
                map(i => i.data),
            );
    }
}

@Injectable()
export class MenuItemService extends CrudService<MenuItemModel> {
    protected serviceItemsKey = 'menuitem';

    constructor(protected store: AppStore) {
        super('MenuItem', null);
    }

    public getAllMenuItems(id) {
        return this.apirequest('get', this.apiCallTo(this.serviceApiUrl + `/Getallbymenuid?menuId=${id}`))
            .pipe(
                map(i => i.data),
                tap(d => this.appStore.set(this.serviceItemsKey, d))
            );
    }

    public deleteMenuItemFile(url:string) {
        return this.apirequest("get", this.apiCallTo('AdminImage/DeactivateMultiple?url='+url)).pipe(
                map(i => i.data),
            );
    }
}