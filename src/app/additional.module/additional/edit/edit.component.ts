import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
//models
import { AdditionalInfoModel } from '../../models/AdditionalInfoModel';
import { AdditionalInfoInsertModel } from '../../models/AdditionalInfoModel';
//services
import { AdditionalService } from "../../services/additional.services";
//components
import { AuthenticatedComponent } from '../../../shared/components/AuthenticatedComponent';
@Component({
	selector: 'app-additional-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class AdditionalEditComponent extends AuthenticatedComponent implements OnInit {

	constructor(
		public additionalService :AdditionalService,
		private activeRoute: ActivatedRoute) {
		super();
	}

	logo;
	additional: AdditionalInfoModel = new AdditionalInfoModel();

	ngOnInit() {

		if (this.isEdit) {

			let additionalId = this.activeRoute.snapshot.params['id'];
			this.additionalService.get(additionalId).subscribe(res => {
				if (!!res) {
					this.additional = res;
				}else {
					this.alertService.showError('Something went wrong!');
					this.back();
				}
			});
		}

	}

	public get isEdit() {
		return this.activeRoute.snapshot.url.join('').includes("edit");
	}

	public logoUpload():Promise<number> {
		return new Promise((resolve, reject) => {
			if(this.logo) {
				this.additionalService.multipleUploadFile(this.logo).subscribe(res => {
					resolve(res.data[0].id);
				});
			} else {
				resolve();
			}
		});
	}

	createInsertModel(additional: AdditionalInfoModel): AdditionalInfoInsertModel {
		let insertModel: AdditionalInfoInsertModel = new AdditionalInfoInsertModel();

		insertModel.id = additional.id;
		insertModel.name = additional.name;
		insertModel.imageIds = additional.imageUrls.map(item => item.id);
		insertModel.logoId = additional.logoId;
		insertModel.coverId = additional.coverId;
		insertModel.nameTranslationId = additional.nameTranslationId;
		insertModel.isActive = additional.isActive;

		return insertModel;
	}

	public save() {

		if (!this.isFormValid()) return;

		let insertModel = this.createInsertModel(this.additional);

		if (!this.additional.id) {

			this.additionalService.save(insertModel).subscribe(data => {

				if(!data.hasError) {

					this.additionalService.updateData({id:data.data}).subscribe(res => {

						this.alertService.showSuccess('Additional Info added successfully.');

						let data = this.createInsertModel(res.data);

						this.logoUpload().then(logoId => {

							if (!!logoId) { data.logoId = logoId }

								if(!!data.logoId ) {

									this.additionalService.save(data).subscribe(d => {
										if(!d.hasError) {
											this.additionalService.updateData({id:data.id}).subscribe(res => {
												this.alertService.showSuccess('Additional Info updated successfully.');
												this.back();
											})
										} else {
											let errors = d.errors.map(r => {return r.message});
											this.alertService.showError(errors);
										}
									});
								} else {
									this.back();
								}
							});
					});
				} else {
					let errors = data.errors.map(r => {return r.message});
					this.alertService.showError(errors);
				}
			})
		} else {

			this.logoUpload().then(logoId => {

				if (!!logoId) { insertModel.logoId = logoId }

				this.additionalService.save(insertModel).subscribe(d => {
					if(!d.hasError) {
						this.additionalService.updateData({id:insertModel.id}).subscribe(res => {
							this.alertService.showSuccess('Additional Info updated successfully.');
							this.back();
						})
					} else {
						let errors = d.errors.map(r => {return r.message});
						this.alertService.showError(errors);
					}
				});
			});
		}
	}

	public isFormValid() {
		if (!super.isFormValid()) {
			this.alertService.showError("The form is not valid");
			return false;
		}
		return true;
	}

	// public back() {
	// 	this.router.navigateByUrl("additional-info/list");
	// }
}
