import { Injectable } from "@angular/core";
import { DataStore } from "../shared/models/DataStore";
import { PlaceService } from "./services/place.service";

@Injectable()
export class PlaceStore extends DataStore {
    constructor() {
        super();
    }

    public initializeStore() {
        this.set('place', null);
    }
}