import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceContactComponent } from './place-contact.component';

describe('PlaceContactComponent', () => {
  let component: PlaceContactComponent;
  let fixture: ComponentFixture<PlaceContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
