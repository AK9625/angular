import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceTablesComponent } from './place-tables.component';

describe('PlaceTablesComponent', () => {
  let component: PlaceTablesComponent;
  let fixture: ComponentFixture<PlaceTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
