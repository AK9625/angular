import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacePersonalComponent } from './place-personal.component';

describe('PlacePersonalComponent', () => {
  let component: PlacePersonalComponent;
  let fixture: ComponentFixture<PlacePersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacePersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacePersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
