import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacePersonalEditComponent } from './edit.component';

describe('PlacePersonalEditComponent', () => {
  let component: PlacePersonalEditComponent;
  let fixture: ComponentFixture<PlacePersonalEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacePersonalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacePersonalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
