import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceDiscountComponent } from './place-discount.component';

describe('PlaceDiscountComponent', () => {
  let component: PlaceDiscountComponent;
  let fixture: ComponentFixture<PlaceDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
