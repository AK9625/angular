import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceSettingsComponent } from './place-settings.component';

describe('PlaceSettingsComponent', () => {
  let component: PlaceSettingsComponent;
  let fixture: ComponentFixture<PlaceSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
