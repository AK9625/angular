import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitializeAppComponent } from './initialize-app.component';

describe('InitializeAppComponent', () => {
  let component: InitializeAppComponent;
  let fixture: ComponentFixture<InitializeAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitializeAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitializeAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
